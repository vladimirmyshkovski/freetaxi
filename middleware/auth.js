export default function(context) {
  const auth = context.app.$user()
  if (auth.is) {
    return true
  } else {
    return context.redirect(
      `/auth/signin?next=${context.app.context.route.path}`
    )
  }
}
