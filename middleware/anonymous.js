export default function(context) {
  const auth = context.app.$user()
  if (auth.is) {
    return context.app.router.push('/')
  }
}
