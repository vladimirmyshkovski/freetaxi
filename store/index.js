export const state = () => ({
  drawer: false,
  loading: false
})

export const mutations = {
  toggleDrawer(state) {
    state.drawer = !state.drawer
  },
  setDrawer(state, value) {
    state.drawer = value
  },
  toggleLoading(state) {
    state.loading = !state.loading
  },
  setLoading(state, value) {
    state.loading = value
  }
}

export const actions = {
  toggleDrawer({ commit }) {
    commit('toggleDrawer')
  },
  setDrawer({ commit }, payload) {
    commit('setDrawer', payload)
  },
  toggleLoading({ commit }) {
    commit('toggleLoading')
  },
  setLoading({ commit }, payload) {
    commit('setLoading', payload)
  }
}
