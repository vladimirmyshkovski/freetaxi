export const state = () => ({
  open: true,
  redirect: false,
  redirectURL: '/',
  outsideClickHanding: true
})

export const mutations = {
  setOpen(state, payload) {
    if (state.open !== payload) {
      if (typeof payload === 'boolean') {
        state.open = payload
      }
    }
  },
  setRedirect(state, payload) {
    if (state.redirect !== payload) {
      if (typeof payload === 'boolean') {
        state.redirect = payload
      }
    }
  },
  setRedirectURL(state, payload) {
    if (state.redirectURL !== payload) {
      if (typeof payload === 'string') {
        state.redirectURL = payload
      }
    }
  },
  setOutsideClickHanding(state, payload) {
    state.outsideClickHanding = payload
  }
}

export const actions = {
  setOpen({ state, commit }, payload) {
    commit('setOpen', payload)
    if (state.redirect) {
      this.$router.push(state.redirectURL || '/')
    }
  },
  setRedirect({ commit }, payload) {
    commit('setRedirect', payload)
  },
  setRedirectURL({ commit }, payload) {
    commit('setRedirectURL', payload)
  },
  setOutsideClickHanding({ commit }, payload) {
    commit('setOutsideClickHanding', payload)
  }
}
