export const state = () => ({
  snackbar: {
    show: false,
    timeout: 3000,
    right: true,
    top: true,
    text: '',
    color: 'primary'
  }
})

export const getters = {
  snackbar: (state) => state.snackbar
}

export const mutations = {
  set(state, payload) {
    for (const key in payload) {
      if (state.snackbar[key] !== undefined) {
        state.snackbar[key] = payload[key]
      }
    }
  },
  pop(state) {
    state.snackbar.show = false
    state.snackbar.text = ''
  }
}

export const actions = {
  set({ commit }, payload) {
    commit('set', payload)
  },
  pop({ commit }) {
    commit('pop')
  }
}
