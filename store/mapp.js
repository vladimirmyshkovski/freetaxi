export const state = () => ({
  map: null,
  zoom: 10,
  options: { zoomControl: false },
  center: [41.9921602333763, 41.77001953125001],
  users: [],
  cars: [],
  orders: [],
  clickedPosition: null
})

export const mutations = {
  pushUser(state, payload) {
    state.users.push(payload)
  },
  setUsers(state, payload) {
    state.users = payload
  },
  pushCar(state, payload) {
    state.cars.push(payload)
  },
  setCars(state, payload) {
    state.cars = payload
  },
  pushOrder(state, payload) {
    state.orders.push(payload)
  },
  setOrders(state, payload) {
    state.orders = payload
  },
  setClickedPosition(state, payload) {
    state.clickedPosition = payload
  },
  setMap(state, payload) {
    state.map = payload
  },
  setCenter(state, payload) {
    if (state.center !== payload) {
      if (typeof state.center === 'object') {
        state.center = payload
      }
    }
  }
}

export const actions = {
  pushUser({ commit }, payload) {
    commit('pushUser', payload)
  },
  setUsers({ commit }, payload) {
    commit('setUsers', payload)
  },
  pushCar({ commit }, payload) {
    commit('pushCar', payload)
  },
  setCars({ commit }, payload) {
    commit('setCars', payload)
  },
  pushOrder({ commit }, payload) {
    commit('pushOrder', payload)
  },
  setOrders({ commit }, payload) {
    commit('setOrders', payload)
  },
  setClickedPosition({ commit }, payload) {
    commit('setClickedPosition', payload)
  },
  setMap({ commit }, payload) {
    commit('setMap', payload)
  },
  setCenter({ commit }, payload) {
    commit('setCenter', payload)
  }
}
