export const actions = {
  signUp({ dispatch, state }, payload) {
    const router = this.$router
    const gun = this.$gun()
    gun.user().create(payload.username, payload.password, function(ack) {
      if ('ok' in ack) {
        gun.get('users').set(gun.user())
        dispatch(
          'snackbar/set',
          {
            show: true,
            color: 'success',
            text: `Hello ${payload.username}!`
          },
          { root: true }
        )
        router.push('/auth/signin')
      } else {
        dispatch(
          'snackbar/set',
          {
            show: true,
            color: 'error',
            text: ack.err
          },
          { root: true }
        )
      }
    })
  },
  signIn({ dispatch, state }, payload) {
    const router = this.$router
    const gun = this.$gun()
    const watchLocation = this.$watchLocation
    const userReference = (event) => {
      if ('ack' in event) {
        dispatch(
          'snackbar/set',
          {
            show: true,
            color: 'success',
            text: `Welcome back, ${payload.username}`
          },
          { root: true }
        )
        gun
          .user()
          .get('online')
          .put(true)
        watchLocation().then((position) => {
          gun
            .user()
            .get('position')
            .put(position)
        })
        router.push(router.history.current.query.next || '/')
      } else {
        dispatch(
          'snackbar/set',
          {
            show: true,
            color: 'error',
            text: event.err
          },
          { root: true }
        )
      }
    }
    this.$gun()
      .user()
      .auth(payload.username, payload.password, userReference)
  },
  signOut() {
    this.$gun()
      .user()
      .leave()
    this.$router.push('/')
  },
  resetPassword({ dispatch }, payload) {
    const { username, password, newPassword } = payload
    const router = this.$router
    this.$gun()
      .user()
      .auth(
        username,
        password,
        function(event) {
          if ('err' in event) {
            dispatch(
              'snackbar/set',
              {
                show: true,
                color: 'error',
                text: event.err
              },
              { root: true }
            )
          } else {
            dispatch(
              'snackbar/set',
              {
                show: true,
                color: 'success',
                text: 'Password successfully changed!'
              },
              { root: true }
            )
            router.push('/auth/profile')
          }
        },
        { change: newPassword }
      )
  },
  async hintsPassword({ dispatch }, phrase) {
    const encrypted = await this.$SEA().encrypt(phrase, this.$user()._.sea)
    console.log('encrypted', encrypted)
    const data = await this.$SEA().sign(encrypted, this.$user()._.sea)
    console.log('data', data)
    const phraseData = JSON.stringify('@' + data)
    await this.$gun()
      .user()
      .get('hints')
      .put({ phrase: phraseData })
      .then()
    await dispatch(
      'snackbar/set',
      {
        show: true,
        color: 'success',
        text: 'Passwords hints successfully saved!'
      },
      { root: true }
    )
    await this.$router.push('/auth/profile')
  },
  recall() {
    const userReference = (event) => {
      return event
    }
    this.$gun()
      .user()
      .recall({ sessionStorage: true }, userReference)
  }
}
