export const state = () => ({
  show: false,
  text: 'Return to complete the order',
  showButton: false,
  buttonText: 'Close',
  buttonFunction: () => ({})
})

export const mutations = {
  set(state, payload) {
    for (const key in payload) {
      if (state[key] !== undefined) {
        state[key] = payload[key]
      }
    }
  }
}

export const actions = {
  set({ commit }, payload) {
    commit('set', payload)
  }
}
