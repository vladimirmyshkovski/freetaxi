export function setCarOwner(car, owner) {
  car.get('owner').set(owner)
}

export function setImagesToCar(car, images) {
  images.forEach(function(image) {
    car.get('images').set(image)
  })
}

export function createCar(gun, carData, orderOwner) {
  const images = carData.images
  delete carData.images
  const userCar = gun
    .user()
    .get('cars')
    .set(carData)
  const car = gun.get('cars').set(carData)
  setCarOwner(car, orderOwner)
  if (images) {
    setImagesToCar(car, images)
    setImagesToCar(userCar, images)
  }
  return car
}

export async function getCars(gun) {
  const cars = []
  await gun
    .user()
    .get('cars')
    .once()
    .map()
    .on(function(car, carPublickKey) {
      if (car && carPublickKey) {
        if (car.image && car.number) {
          const newCar = car
          newCar.id = carPublickKey
          if (cars.some((someCar) => someCar.id === car.id)) {
            const foundIndex = cars.findIndex((x) => x.id === car.id)
            cars[foundIndex] = car
          } else {
            cars.push(newCar)
          }
        }
      }
    })
    .then()
  return cars
}

export async function activateCar(gun, carPublickKey) {
  const car = await gun
    .user()
    .get('cars')
    .once()
    .map()
    .once(function(data, publickKey) {
      this.put({ active: carPublickKey === publickKey })
    })
    .then()
  return car
}

export async function deactivateCar(gun, carPublickKey) {
  const car = await gun
    .user()
    .get('cars')
    .get(carPublickKey)
    .put({ active: false })
    .then()
  return car
}

export async function getCar(gun, carPublickKey) {
  const car = await gun
    .user()
    .get('cars')
    .get(carPublickKey)
    .then()
  return car
}

export function getCarImages(gun, carPublickKey) {
  const images = []
  gun
    .user()
    .get('cars')
    .get(carPublickKey)
    .get('images')
    .once()
    .map()
    .on(function(image) {
      if (image) {
        images.push(image)
      }
    })
    .then()
  return images
}

export async function deleteCar(gun, carPublickKey) {
  const car = await gun
    .user()
    .get('cars')
    .get(carPublickKey)
    .put({ removed: true })
    .put(null)
    .then()
  return car
}
