export function createNotification(gun, notificationData) {
  return gun
    .user()
    .get('notifications')
    .set(notificationData)
}
