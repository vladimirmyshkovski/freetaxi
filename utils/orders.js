export function createOrder(gun, orderData, orderOwner) {
  const order = gun.get('orders').set(orderData)
  gun
    .user()
    .get('orders')
    .set(orderData)
  order.get('owner').set(orderOwner)
  return order
}

export function getUsersOrders(gun) {
  const orders = []
  gun
    .user()
    .get('orders')
    .map()
    .once((data, id) => {
      if (
        data &&
        id &&
        typeof data === 'object' &&
        typeof id === 'string' &&
        'carType' in data
      ) {
        const order = data
        order.id = id
        orders.push(order)
      }
    })
  return orders
}

export function getUsersOrder(gun, orderPub) {
  let order
  gun
    .user()
    .get('orders')
    .get(orderPub)
    .once((data, id) => {
      if (
        data &&
        id &&
        typeof data === 'object' &&
        typeof id === 'string' &&
        'carType' in data
      ) {
        order = data
        order.id = id
      }
    })
  return order
}

export function getOrders(gun) {
  const orders = []
  gun
    .get('orders')
    .map()
    .once((data, id) => {
      if (
        data &&
        id &&
        typeof data === 'object' &&
        typeof id === 'string' &&
        'carType' in data
      ) {
        const order = data
        order.id = id
        gun
          .get('orders')
          .get(id)
          .get('startPoint')
          .once((data) => (order.startPlace = data))
          .back()
          .get('endPoint')
          .once((data) => (order.endPlace = data))
        if (
          order.startPlace &&
          order.startPlace.lat &&
          order.startPlace.lon &&
          order.endPlace &&
          order.endPlace.lat &&
          order.endPlace.lon
        ) {
          orders.push(order)
        }
      }
    })
  return orders
}

export function getOrder(gun, orderPub) {
  let order
  gun
    .get('orders')
    .get(orderPub)
    .once((data, id) => {
      console.log('order', order)
      if (
        data &&
        id &&
        typeof data === 'object' &&
        typeof id === 'string' &&
        'carType' in data
      ) {
        order = data
        order.id = id
      }
    })
  return order
}
