const bitcore = require('bitcore-lib')

export function getAddresses(gun) {
  const addresses = []
  gun
    .user()
    .get('wallet')
    .get('addresses')
    .map()
    .once(function(data, id) {
      const address = data
      const exists = addresses.some(function(item) {
        return address.address === item.address
      })
      if (!exists && address.private) {
        address.id = id
        addresses.push(address)
      }
    })
  return addresses
}

export function getAddress(gun, addressPub) {
  return gun
    .user()
    .get('wallet')
    .get('addresses')
    .get(addressPub)
}

export function generateAddress(gun) {
  const privateKey = new bitcore.PrivateKey()
  const address = privateKey.toAddress()
  return gun
    .user()
    .get('wallet')
    .get('addresses')
    .set({
      private: privateKey.toString(),
      address: address.toString()
    })
}

export function getAddressBalance(address) {
  const URL = `https://test-insight.bitpay.com/api/addr/${address}/balance`
  return fetch(URL).then((response) => {
    return response.json().then((response) => {
      return response
    })
  })
}

export function updateAddressBalance(gun, address, addressPub) {
  return getAddressBalance(address).then((data) => {
    return gun
      .user()
      .get('wallet')
      .get('addresses')
      .get(addressPub)
      .get('balance')
      .put(data)
  })
}

export function getRate(code) {
  return fetch('https://bitpay.com/api/rates').then((response) => {
    return response.then((response) => {
      response.forEach(function(item) {
        if (item.code === code.toUpperCase()) {
          return item.rate
        }
      })
    })
  })
}

export function sendTransaction(rawtx) {
  if (rawtx) {
    const URL = `https://test-insight.bitpay.com/api/tx/send`
    return fetch(URL, {
      method: 'post',
      body: JSON.stringify({
        rawtx
      })
    })
      .then((response) => {
        console.log('response in sendTransaction', response)
      })
      .catch((error) => console.log('error', error))
  }
}

export function getUnspendTransactions(address) {
  console.log('address on getUnspendTransactions', address)
  const URL = `https://test-insight.bitpay.com/api/addr/${address}/utxo`
  return fetch(URL).then((response) => {
    console.log('getUnspendTransactions', response)
    return response.json()
  })
}

export function buildRawTransaction(toAddress, fromPrivateKey, satoshiAmount) {
  const privateKey = bitcore.PrivateKey.fromWIF(fromPrivateKey)
  const sourceAddress = privateKey.toAddress(bitcore.Networks.testnet)
  const targetAddress = bitcore.Address.fromString(toAddress)
  return getUnspendTransactions(sourceAddress.toString()).then((utxos) => {
    if (utxos.length > 0) {
      /*
      const utxos = [
        {
          address: 'mywRqUpbENhbu5VsYDwiMTJouVK9g2ZEJQ',
          txid: '761693565e82ca176532c52a37fb38cd9f1eb0172a00562b394e60ede0b7df8a',
          vout: 1,
          scriptPubKey: '76a914ca133ceac705b723b91263aa163ea8a45954e49a88ac',
          amount: 0.0001,
          satoshis: 10000,
          height: 1578273,
          confirmations: 338
        }
      ]
      */
      const tx = new bitcore.Transaction()
      tx.from(utxos)
      tx.to(targetAddress, Number(satoshiAmount))
      tx.change(sourceAddress)
      tx.sign(privateKey)
      const serializedTX = tx.serialize()
      return serializedTX
    }
  })
  // insight.getUnspentUtxos(sourceAddress, function(error, utxos) {
  //   console.log('error', error)
  //   console.log('utxos', utxos)
  // })
  /*
  if (!bitcore.Address.isValid(targetAddress)) {
    console.log('toAddress invalid')
  } else {
    console.log('toAddress valid')
  }
  insight.getUnspentUtxos(sourceAddress, function(error, utxos) {
    if (error) {
      console.log(error)
    } else {
      console.log(utxos)
      // transaction code goes here
      const tx = new bitcore.Transaction()
      tx.from(utxos)
      tx.to(targetAddress, satoshiAmount)
      tx.change(sourceAddress)
      tx.sign(privateKey)
      const serializedTX = tx.serialize()
      insight.broadcast(serializedTX, function(error, transactionId) {
        if (error) {
          console.log('broadcast error', error)
        } else {
          console.log('transactionId', transactionId)
        }
      })
    }
  })
  */
}

export function buildRawTransactions(
  fromAddress,
  toAddress,
  fromPrivateKey,
  satoshiAmount
) {
  // const key = bitcoin.ECPair.fromWIF(fromWif, testnet)
  // const transaction = new bitcoin.Psbt({ network: testnet })
  getUnspendTransactions(fromAddress)
    .then((utxo) => {
      console.log('utxo', utxo)
      if (utxo.length > 0) {
        /*
        const txid = utxo[utxo.length - 1].txid
        if (txid) {
          console.log('txid', txid)
          transaction.addInput({
            hash: txid,
            index: 0 // ,
            // nonWitnessUtxo: Buffer.from(
            //   '1ce642c65f07de93ff49ff9b4b415cae0230044021546677fe920e7d831e0d5a',
            //   'hex'
            // )
          })
          console.log('key', key)
          console.log('toAddress', toAddress)
          console.log('satoshiAmount', satoshiAmount)
          const script = Buffer.from(toAddress, 'hex')
          console.log('script', script)
          transaction.addOutput({
            script,
            value: Number(satoshiAmount)
          })
          transaction.signInput(0, key)
          transaction.validateSignaturesOfInput(0)
          transaction.finalizeAllInputs()
          return transaction.extractTransaction().toHex()
        }
        */
        const privateKey = new bitcore.PrivateKey(fromPrivateKey)
        console.log('fromPrivateKey', fromPrivateKey)
        const transaction = new bitcore.Transaction()
          .from(utxo.length - 1)
          .to(toAddress, satoshiAmount)
          .sign(privateKey)
        console.log('transaction', transaction)
        return transaction
      }
    })
    .catch((error) => {
      console.log('error', error)
    })
}
