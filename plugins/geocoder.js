const { NominatimJS } = require('@owsas/nominatim-js')

export default ({ app }, inject) => {
  inject('nominatim', () => NominatimJS)
}
