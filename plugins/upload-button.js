import Vue from 'vue'
import UploadButton from 'vuetify-upload-button'

Vue.component('upload-button', UploadButton)
