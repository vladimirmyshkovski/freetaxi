const watchLocation = function(options = {}, forceReject = false) {
  return new Promise((resolve, reject) => {
    if (forceReject) {
      reject(new Error('reject forced for testing purposes'))
      return
    }
    window.navigator.geolocation.watchPosition(
      (position) => {
        resolve({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          altitude: position.coords.altitude,
          altitudeAccuracy: position.coords.altitudeAccuracy,
          accuracy: position.coords.accuracy,
          heading: position.coords.heading,
          speed: position.coords.speed,
          timestamp: position.timestamp
        })
      },
      () => {
        reject(new Error('no position access'))
      },
      options
    )
  })
}

export default ({ app }, inject) => {
  inject('watchLocation', () => watchLocation())
}
