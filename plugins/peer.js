import Peer from 'peerjs'

const peer = new Peer({
  host: 'localhost',
  port: 9000,
  path: '/',
  debug: 1
})

export default ({ app }, inject) => {
  /*
  peer.on('open', function(peerId) {
    app
      .$gun()
      .get('peers')
      .set(peerId)
    app
      .$gun()
      .get('peers')
      .map()
      .once(function(peerId, publickKey) {
        console.log('peerId', peerId, 'publickKey', publickKey)
        if (peerId !== peer.id) {
          try {
            const connect = peer.connect(peerId)
            connect.on('open', function() {
              connect.on('data', function(data) {
                console.log(`Received on ${peer.id} from ${peerId}`, data)
              })
              connect.send(`Hello world from ! ${peer.id}`)
            })
          } catch (err) {
            console.log('err', err)
            app
              .$gun()
              .get('peers')
              .get(publickKey)
              .set(null)
          }
        }
      })
  })
  peer.on('close', function() {
    app
      .$gun()
      .get('peers')
      .get(peer.id)
      .set(null)
  })
  peer.on('disconnect', function() {
    app
      .$gun()
      .get('peers')
      .get(peer.id)
      .set(null)
  })
  */
  inject('peer', () => peer)
}
