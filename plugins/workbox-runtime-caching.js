/* eslint-disable */

workbox.routing.registerRoute(
  /static\/(.*)/,
  workbox.strategies.staleWhileRevalidate({
    cacheName: 'static'
  })
)
workbox.routing.registerRoute(
  /assets\/(.*)/,
  workbox.strategies.staleWhileRevalidate({
    cacheName: 'assets'
  })
)
workbox.routing.registerRoute(
  /\.(?:png|gif|jpg|svg)$/,
  workbox.strategies.staleWhileRevalidate({
    cacheName: 'images-cache'
  })
)
workbox.routing.registerRoute(
  /\.(?:js|css)$/,
  workbox.strategies.staleWhileRevalidate({
    cacheName: 'static-resources'
  })
)
