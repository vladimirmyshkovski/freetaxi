/* eslint-disable */

workbox.routing.registerRoute(
  new RegExp('https://nominatim.openstreetmap.org/.*'),
  new workbox.strategies.StaleWhileRevalidate({
    cacheName: 'nominatim',
    cacheExpiration: {
      maxAgeInSeconds: 60 * 60 * 24
    },
    plugins: [
      new workbox.cacheableResponse.Plugin({
        statuses: [200, 404]
      })
    ]
  })
)

workbox.routing.registerRoute(
  new RegExp('https://vpic.nhtsa.dot.gov/.*'),
  new workbox.strategies.StaleWhileRevalidate({
    cacheName: 'vpic',
    cacheExpiration: {
      maxAgeInSeconds: 60 * 60 * 24 * 365
    },
    plugins: [
      new workbox.cacheableResponse.Plugin({
        statuses: [200, 404]
      })
    ]
  })
)

workbox.routing.registerRoute(
  new RegExp('https://router.project-osrm.org/.*'),
  new workbox.strategies.StaleWhileRevalidate({
    cacheName: 'routing-machine',
    cacheExpiration: {
      maxAgeInSeconds: 60 * 60 * 24
    },
    plugins: [
      new workbox.cacheableResponse.Plugin({
        statuses: [200, 404]
      })
    ]
  })
)
