import Fingerprint2 from 'fingerprintjs2'

let fingerprint

Fingerprint2.get({}, function(components) {
  const values = components.map(function(component) {
    return component.value
  })
  fingerprint = Fingerprint2.x64hash128(values.join(''), 31)
})

export default ({ app }, inject) => {
  inject('fingerprint', () => fingerprint)
}
