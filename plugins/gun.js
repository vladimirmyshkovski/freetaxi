import Gun from 'gun/gun'
import 'gun/axe'
import 'gun/sea'
import 'gun/nts'
import 'gun/lib/then'
import 'gun/lib/not'
import 'gun/lib/rfs'
import 'gun/lib/radix'
import 'gun/lib/radisk'
import 'gun/lib/store'
import 'gun/lib/rindexed'
import 'gun/lib/open'
import 'gun/lib/load'
import 'gun/lib/bye'
import 'gun/lib/shim'
import 'gun/lib/webrtc'

const gun = Gun({
  localStorage: false,
  radisk: true,
  peers: ['http://localhost:8765/gun']
})

export default ({ app }, inject) => {
  inject('gun', () => gun)
  inject('SEA', () => Gun.SEA)
  inject('user', () =>
    gun.user().on(function(user) {
      return user
    })
  )
}
