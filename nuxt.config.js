import colors from 'vuetify/es5/util/colors'

export default {
  mode: 'universal',
  /*
   ** Headers of the page
   */
  head: {
    titleTemplate: '%s - ' + process.env.npm_package_name,
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || ''
      }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#fff' },
  /*
   ** Global CSS
   */
  css: [],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    { src: '@/plugins/gun.js' },
    { src: '@/plugins/geocoder.js' },
    { src: '@/plugins/portal.js' },
    { src: '@/plugins/fingerprint.js', mode: 'client' },
    // { src: '@/plugins/peer.js', mode: 'client' },
    { src: '@/plugins/geolocation.js', mode: 'client' },
    { src: '@/plugins/clipboard.js', mode: 'client' },
    { src: '@/plugins/upload-button.js', mode: 'client' }
  ],
  /*
   ** Nuxt.js dev-modules
   */
  devModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
    '@nuxtjs/vuetify'
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    '@nuxtjs/pwa',
    'nuxt-leaflet',
    'nuxt-webfontloader',
    'portal-vue/nuxt'
  ],
  /*
   ** vuetify module configuration
   ** https://github.com/nuxt-community/vuetify-module
   */
  webfontloader: {
    google: {
      families: ['Roboto:300,400,500,600,700', 'Material Icons'] // Loads Lato font with weights 400 and 700
    }
  },
  /*
   ** workbox module configuration
   ** https://pwa.nuxtjs.org/modules/workbox.html#options
   */
  workbox: {
    dev: false,
    offlineStrategy: 'StaleWhileRevalidate',
    cachingExtensions: [
      '@/plugins/workbox-range-request.js',
      '@/plugins/workbox-response-caching.js'
    ],
    runtimeCaching: [
      {
        urlPattern: 'https://fonts.googleapis.com/.*',
        handler: 'staleWhileRevalidate',
        method: 'GET',
        strategyOptions: {
          cacheableResponse: { statuses: [0, 200] },
          cacheExpiration: {
            maxEntries: 10 * 10 * 10 * 10 * 10,
            maxAgeSeconds: 60 * 60 * 24 * 30 * 365
          }
        }
      },
      {
        urlPattern: 'https://fonts.gstatic.com/.*',
        handler: 'staleWhileRevalidate',
        method: 'GET',
        strategyOptions: {
          cacheableResponse: { statuses: [0, 200] },
          cacheExpiration: {
            maxEntries: 10 * 10 * 10 * 10 * 10,
            maxAgeSeconds: 60 * 60 * 24 * 30 * 365
          }
        }
      },
      {
        urlPattern: 'https://.*.tile.osm.org/.*',
        handler: 'staleWhileRevalidate',
        method: 'GET',
        strategyOptions: {
          cacheableResponse: { statuses: [0, 200] },
          cacheExpiration: {
            maxEntries: 10 * 10 * 10 * 10 * 10,
            maxAgeSeconds: 60 * 60 * 24 * 30 * 365
          }
        }
      },
      {
        urlPattern: 'https://cdn.jsdelivr.net/npm/.*',
        handler: 'staleWhileRevalidate',
        method: 'GET',
        strategyOptions: {
          cacheableResponse: { statuses: [0, 200] },
          cacheExpiration: {
            maxEntries: 10 * 10 * 10 * 10 * 10,
            maxAgeSeconds: 60 * 60 * 24 * 30 * 365
          }
        }
      },
      {
        urlPattern: 'https://cdnjs.cloudflare.com/.*',
        handler: 'staleWhileRevalidate',
        method: 'GET',
        strategyOptions: {
          cacheableResponse: { statuses: [0, 200] },
          cacheExpiration: {
            maxEntries: 10 * 10 * 10 * 10 * 10,
            maxAgeSeconds: 60 * 60 * 24 * 30 * 365
          }
        }
      }
    ]
  },
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      dark: false,
      themes: {
        dark: {
          primary: colors.amber,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        }
      }
    }
  },
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {
      config.node = {
        fs: 'empty'
      }
    }
  }
}
